﻿using System;
using System.Collections.Generic;

namespace FlaviusJosephTask
{
    /// <summary>
    /// Represent class FlaviousJoseph.
    /// </summary>
    public static class FlaviousJoseph
    {
        /// <summary>
        /// Find warrior, which will alive this round.
        /// </summary>
        /// <param name="wariors">Array of warriors of Joseph.</param>
        /// <param name="k">Which warrior should die.</param>
        /// <returns>Alived warior.</returns>
        /// <exception cref="ArgumentException">, when k or count less than 1.</exception>
        public static int GetAlive(int count, int k)
        {
            if (count < 1)
            {
                throw new ArgumentException(nameof(count));
            }

            if (k < 1)
            {
                throw new ArgumentException(nameof(k));
            }

            int res = 0;
            for (int i = 1; i <= count; i++)
            {
                res = (res + k) % i;
            }

            return res + 1;
        }
    }
}
