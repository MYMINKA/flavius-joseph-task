using NUnit.Framework;
using System;

namespace FlaviusJosephTask.Tests
{
    public class FlaviusJosephTests
    {
        [TestCase(5, 2, ExpectedResult = 3)]
        [TestCase(5, 3, ExpectedResult = 4)]
        [TestCase(5, 5, ExpectedResult = 2)]
        [TestCase(7, 2, ExpectedResult = 7)]
        [TestCase(7, 3, ExpectedResult = 4)]
        [TestCase(7, 5, ExpectedResult = 6)]
        [TestCase(10, 2, ExpectedResult = 5)]
        [TestCase(10, 3, ExpectedResult = 4)]
        [TestCase(10, 5, ExpectedResult = 3)]
        public int GetFlaviousJosephTests(int wariors, int k)
        {
            return FlaviousJoseph.GetAlive(wariors, k);
        }

        [TestCase(-123)]
        [TestCase(-1)]
        [TestCase(0)]
        public void GetFlaviousJoseph_CountIsLessThan1_ArgumentException(int count)
        {
            Assert.Throws<ArgumentException>(() => FlaviousJoseph.GetAlive(count, 2));
        }

        [TestCase(-123)]
        [TestCase(-1)]
        [TestCase(0)]
        public void GetFlaviousJoseph_KIsLessThan1_ArgumentException(int k)
        {
            Assert.Throws<ArgumentException>(() => FlaviousJoseph.GetAlive(4, k));
        }
    }
}